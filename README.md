# TESTE FRONT-END SODIT #

O teste é bem simples e consiste em montar a página abaixo utilizando angular, de preferência a versão 4.x
### [https://marvelapp.com/9dhe13e/screen/31660626](https://marvelapp.com/9dhe13e/screen/31660626) ####

## Importante: ##

* Não é necessário fazer funcionar o slider
* Usar typescript será um diferencial
* Utilizar SASS ou LESS será bem visto o uso de Flexbox também
* Tem que ser responsivo
* O prazo é de 7 dias corridos
* Pode consultar o Google
* Na pasta do projeto estão as fontes e as imagens

### Observação: ###

* Esse é um teste de uso único e exclusivo da SODIT.

## BOA SORTE! ##